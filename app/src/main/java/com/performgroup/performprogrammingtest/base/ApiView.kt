package com.performgroup.performprogrammingtest.base

import com.hannesdorfmann.mosby3.mvp.MvpView

interface ApiView : MvpView {
    fun onError()

    fun onNoInternetConnection()
}