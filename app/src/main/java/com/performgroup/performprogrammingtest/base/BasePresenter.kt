package com.performgroup.performprogrammingtest.base

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.disposables.Disposable
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class BasePresenter<V : ApiView> : MvpBasePresenter<V>() {

    lateinit var disposable: Disposable

    fun disposeApiRequest() {
        if (this::disposable.isInitialized && !this.disposable.isDisposed) {
            this.disposable.dispose()
        }
    }

    fun handlingError(throwable: Throwable) {
        if (throwable is SocketTimeoutException || throwable is UnknownHostException) {
            ifViewAttached { view -> view.onNoInternetConnection() }
        } else {
            ifViewAttached { view -> view.onError() }
        }
    }

    override fun detachView() {
        this.disposeApiRequest()
        super.detachView()
    }
}