package com.performgroup.performprogrammingtest.base

import android.support.v7.widget.RecyclerView

abstract class BaseAdapter<T : RecyclerView.ViewHolder, M> : RecyclerView.Adapter<T>() {

    protected var modelList: MutableList<M> = ArrayList()

    fun addFromResponse(modelsList: List<M>) {
        this.modelList.clear()
        this.modelList.addAll(modelsList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = modelList.size

}
