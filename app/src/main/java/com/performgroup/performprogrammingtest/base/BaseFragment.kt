package com.performgroup.performprogrammingtest.base

import android.support.design.widget.Snackbar
import android.view.View
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.performgroup.performprogrammingtest.R


abstract class BaseFragment<V : ApiView, P : BasePresenter<V>> : MvpFragment<V, P>() {

    abstract fun showLoader(isLoading: Boolean)

    fun getErrorSnackbar(rootView: View) = Snackbar.make(rootView, R.string.connection_error, Snackbar.LENGTH_INDEFINITE)

    fun getNoInternetSnackbar(rootView: View) = Snackbar.make(rootView, R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE)

}

