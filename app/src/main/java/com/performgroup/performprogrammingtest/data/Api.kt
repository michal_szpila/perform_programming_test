package com.performgroup.performprogrammingtest.data

import com.performgroup.performprogrammingtest.news.model.Rss
import com.performgroup.performprogrammingtest.scores.model.Gsmrs
import com.performgroup.performprogrammingtest.standings.model.ResultGsmrs
import io.reactivex.Observable
import retrofit2.http.GET

interface Api {

    @GET("utilities/interviews/techtest/latestnews.xml")
    fun getNews(): Observable<Rss>

    @GET("utilities/interviews/techtest/scores.xml")
    fun getScores(): Observable<Gsmrs>

    @GET("utilities/interviews/techtest/standings.xml")
    fun getStandings(): Observable<ResultGsmrs>

}