package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "method", strict = false)
data class Method(

        @get:Attribute(name = "method_id")
        @set:Attribute(name = "method_id")
        var methodId: Long? = null,

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @field:ElementList(inline = true, entry = "parameter")
        var paramaters: List<Paramater>? = null
)