package com.performgroup.performprogrammingtest.scores


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.PerformTestApplication
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseFragment
import com.performgroup.performprogrammingtest.dagger.NetModule
import com.performgroup.performprogrammingtest.scores.model.Match
import kotlinx.android.synthetic.main.fragment_scores.*



class ScoresFragment : BaseFragment<ScoresView, ScoresPresenter>(), ScoresView {

    private lateinit var scoresAdapter: ScoresAdapter

    override fun createPresenter(): ScoresPresenter {
        return DaggerScoresComponent.builder()
                .appComponent(PerformTestApplication.component)
                .netModule(NetModule())
                .build()
                .presenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scores, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.initAdapter()

        this.showLoader(true)
        this.showFetchingNewData(false)

        this.presenter.getScoresFromApi()
    }

    private fun initAdapter() {
        this.scoresAdapter = ScoresAdapter()
        this.scores_recycler_view.layoutManager = LinearLayoutManager(activity)
        this.scores_recycler_view.adapter = this.scoresAdapter
    }

    override fun onMatchesLoaded(date: String, matches: List<Match>) {
        this.scores_date.text = date
        this.scoresAdapter.addFromResponse(matches)

        this.showLoader(false)
        this.showFetchingNewData(false)

    }

    override fun onError() {
        this.showLoader(false)

        this.getErrorSnackbar(this.scores_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getScoresFromApi()
        }.show()
    }

    override fun onNoInternetConnection() {
        this.showLoader(false)

        this.getNoInternetSnackbar(this.scores_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getScoresFromApi()
        }.show()
    }

    override fun onStartFetchingData(isFetching: Boolean) {
        this.showFetchingNewData(isFetching)
    }


    override fun showLoader(isLoading: Boolean) {
        if (isLoading) {
            this.scores_loader.visibility = View.VISIBLE
            this.scores_container.visibility = View.GONE
        } else {
            this.scores_loader.visibility = View.GONE
            this.scores_container.visibility = View.VISIBLE
        }
    }

    private fun showFetchingNewData(isFetching: Boolean) {
        if (isFetching) {
            this.scores_refreshing.visibility = View.VISIBLE
        } else {
            this.scores_refreshing.visibility = View.GONE
        }
    }


}
