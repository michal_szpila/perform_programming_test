package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "group", strict = false)
data class Group(

        @get:Attribute(name = "group_id")
        @set:Attribute(name = "group_id")
        var groupId: Long? = null,

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @get:Attribute(name = "details")
        @set:Attribute(name = "details")
        var details: String? = null,

        @get:Attribute(name = "winner")
        @set:Attribute(name = "winner")
        var winner: String? = null,

        @get:Attribute(name = "last_updated")
        @set:Attribute(name = "last_updated")
        var lastUpdated: String? = null,

        @field:ElementList(inline = true, entry = "match")
        var matches: List<Match>? = null

)