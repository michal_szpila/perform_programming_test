package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "gsmrs", strict = false)
data class Gsmrs(

        @get:Attribute(name = "version")
        @set:Attribute(name = "version")
        var version: String? = null,

        @get:Attribute(name = "sport")
        @set:Attribute(name = "sport")
        var sport: String? = null,

        @get:Attribute(name = "lang")
        @set:Attribute(name = "lang")
        var lang: String? = null,

        @get:Attribute(name = "last_generated")
        @set:Attribute(name = "last_generated")
        var lastGenerated: String? = null,

        @field:Element(name = "competition")
        var competition: Competition? = null,

        @field:Element(name = "method")
        var method: Method? = null
)