package com.performgroup.performprogrammingtest.scores

import com.performgroup.performprogrammingtest.base.BasePresenter
import com.performgroup.performprogrammingtest.data.Api
import com.performgroup.performprogrammingtest.scores.model.Group
import com.performgroup.performprogrammingtest.scores.model.Gsmrs
import com.performgroup.performprogrammingtest.scores.model.Match
import com.performgroup.performprogrammingtest.scores.model.Paramater
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ScoresPresenter @Inject constructor(private val api: Api) : BasePresenter<ScoresView>() {


    fun getScoresFromApi() {
        this.disposeApiRequest()

        this.disposable = this.api.getScores()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ gsmrs ->
                    ifViewAttached { view -> view.onMatchesLoaded(this.getDateFromGsmrs(gsmrs), this.getMatchListFromGsmrs(gsmrs)) }
                }, { throwable ->
                    throwable.printStackTrace()
                    this.handlingError(throwable)
                }, { this.refresh() })
    }

    fun refresh() {
        this.disposeApiRequest()

        this.disposable = Observable.timer(30, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap {
                    ifViewAttached { view -> view.onStartFetchingData(true) }
                    api.getScores().subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ gsmrs ->
                    ifViewAttached { view -> view.onMatchesLoaded(this.getDateFromGsmrs(gsmrs), this.getMatchListFromGsmrs(gsmrs)) }
                }, { throwable ->
                    throwable.printStackTrace()
                    this.refresh()
                }, { this.refresh() })


    }

    fun getDateFromGsmrs(gsmrs: Gsmrs): String {
        var date = ""
        for (parameter: Paramater in gsmrs!!.method!!.paramaters!!) {
            if (parameter.name == "date") {
                date = parameter!!.value!!
                break
            }
        }

        return date
    }

    fun getMatchListFromGsmrs(gsmrs: Gsmrs): ArrayList<Match> {
        val matchList = ArrayList<Match>()
        for (group: Group in gsmrs!!.competition!!.season!!.round!!.groupList!!) {
            for (match: Match in group!!.matches!!) {
                match.group = group!!.name!!
                matchList.add(match)
            }
        }

        return matchList
    }


}