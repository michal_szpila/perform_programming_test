package com.performgroup.performprogrammingtest.scores

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseAdapter
import com.performgroup.performprogrammingtest.scores.model.Match
import kotlinx.android.synthetic.main.recycler_view_scores_row.view.*

class ScoresAdapter : BaseAdapter<ScoresAdapter.ScoresViewHolder, Match>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ScoresViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_scores_row, null, false)
        return ScoresViewHolder(view)
    }

    override fun onBindViewHolder(holder: ScoresViewHolder, position: Int) {
        holder.bind(this.modelList[position], position, this.shouldShowGroupName(position))
    }

    private fun shouldShowGroupName(position: Int): Boolean {
        return if (position == 0) {
            true
        } else this.modelList[position].group != this.modelList[position - 1].group
    }

    class ScoresViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(match: Match, position: Int, showGroupName: Boolean) {

            this.itemView.scores_row_first_team.text = match.teamAName
            this.itemView.scores_row_second_team.text = match.teamBName
            this.itemView.scores_row_result.text = this.itemView.context.getString(R.string.scores_row_result_holder, match.fsA, match.fsB)

            if (position % 2 == 0) {
                this.itemView.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorFirstRow))
            } else {
                this.itemView.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorSecondRow))
            }

            if (showGroupName) {
                this.itemView.scores_row_group_name.text = match.group
                this.itemView.scores_row_group_container.visibility = View.VISIBLE
            } else {
                this.itemView.scores_row_group_container.visibility = View.GONE
            }
        }

    }
}