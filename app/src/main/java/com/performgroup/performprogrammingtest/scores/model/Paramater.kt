package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "parameter", strict = false)
data class Paramater (

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @get:Attribute(name = "value")
        @set:Attribute(name = "value")
        var value: String? = null
)