package com.performgroup.performprogrammingtest.scores

import com.performgroup.performprogrammingtest.base.ApiView
import com.performgroup.performprogrammingtest.scores.model.Match

interface ScoresView : ApiView {

    fun onMatchesLoaded(date: String, matches: List<Match>)

    fun onStartFetchingData(isFetching: Boolean)
}