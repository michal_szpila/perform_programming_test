package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "season", strict = false)
data class Season(

        @get:Attribute(name = "season_id")
        @set:Attribute(name = "season_id")
        var seasonId: Long? = null,

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @get:Attribute(name = "start_date")
        @set:Attribute(name = "start_date")
        var startDate: String? = null,

        @get:Attribute(name = "end_date")
        @set:Attribute(name = "end_date")
        var endDate: String? = null,

        @get:Attribute(name = "service_level")
        @set:Attribute(name = "service_level")
        var serviceLevel: String? = null,

        @get:Attribute(name = "last_updated")
        @set:Attribute(name = "last_updated")
        var lastUpdated: String? = null,

        @field:Element(name = "round")
        var round: Round? = null
)