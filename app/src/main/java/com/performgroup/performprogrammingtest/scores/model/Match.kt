package com.performgroup.performprogrammingtest.scores.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "match", strict = false)
data class Match(

        @get:Attribute(name = "match_id")
        @set:Attribute(name = "match_id")
        var matchId: Long? = null,

        @get:Attribute(name = "date_utc")
        @set:Attribute(name = "date_utc")
        var dateUtc: String? = null,

        @get:Attribute(name = "time_utc")
        @set:Attribute(name = "time_utc")
        var timeUtc: String? = null,

        @get:Attribute(name = "date_london")
        @set:Attribute(name = "date_london")
        var dateLondon: String? = null,

        @get:Attribute(name = "time_london")
        @set:Attribute(name = "time_london")
        var timeLondon: String? = null,

        @get:Attribute(name = "team_A_id")
        @set:Attribute(name = "team_A_id")
        var teamAId: Long? = null,

        @get:Attribute(name = "team_A_name")
        @set:Attribute(name = "team_A_name")
        var teamAName: String? = null,

        @get:Attribute(name = "team_A_country")
        @set:Attribute(name = "team_A_country")
        var teamACountry: String? = null,

        @get:Attribute(name = "team_B_id")
        @set:Attribute(name = "team_B_id")
        var teamBId: Long? = null,

        @get:Attribute(name = "team_B_name")
        @set:Attribute(name = "team_B_name")
        var teamBName: String? = null,

        @get:Attribute(name = "team_B_country")
        @set:Attribute(name = "team_B_country")
        var teamBCountry: String? = null,

        @get:Attribute(name = "status")
        @set:Attribute(name = "status")
        var status: String? = null,

        @get:Attribute(name = "gameweek")
        @set:Attribute(name = "gameweek")
        var gameWeek: String? = null,

        @get:Attribute(name = "winner")
        @set:Attribute(name = "winner")
        var winner: String? = null,

        @get:Attribute(name = "fs_A")
        @set:Attribute(name = "fs_A")
        var fsA: String? = null,

        @get:Attribute(name = "fs_B")
        @set:Attribute(name = "fs_B")
        var fsB: String? = null,

        @get:Attribute(name = "hts_A")
        @set:Attribute(name = "hts_A")
        var htsA: String? = null,

        @get:Attribute(name = "hts_B")
        @set:Attribute(name = "hts_B")
        var htsB: String? = null,

        @get:Attribute(name = "ets_A")
        @set:Attribute(name = "ets_A")
        var etsA: String? = null,

        @get:Attribute(name = "ets_B")
        @set:Attribute(name = "ets_B")
        var etsB: String? = null,

        @get:Attribute(name = "ps_A")
        @set:Attribute(name = "ps_A")
        var psA: String? = null,

        @get:Attribute(name = "ps_B")
        @set:Attribute(name = "ps_B")
        var psB: String? = null,

        @get:Attribute(name = "last_updated")
        @set:Attribute(name = "last_updated")
        var lastUpdated: String? = null,

        var group: String = ""

)