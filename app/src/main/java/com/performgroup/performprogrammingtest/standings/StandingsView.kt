package com.performgroup.performprogrammingtest.standings

import com.performgroup.performprogrammingtest.base.ApiView
import com.performgroup.performprogrammingtest.standings.model.Ranking

interface StandingsView : ApiView {

    fun onRankingLoaded(ranking: List<Ranking>)
}