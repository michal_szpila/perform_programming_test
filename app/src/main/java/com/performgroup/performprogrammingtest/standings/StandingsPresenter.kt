package com.performgroup.performprogrammingtest.standings

import com.performgroup.performprogrammingtest.base.BasePresenter
import com.performgroup.performprogrammingtest.dagger.PerScreen
import com.performgroup.performprogrammingtest.data.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@PerScreen
class StandingsPresenter @Inject constructor(private val api: Api) : BasePresenter<StandingsView>() {


    fun getStandingsFromApi() {
        this.disposeApiRequest()

        this.disposable = this.api.getStandings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ resultGsmrs ->
                    val sortedRanking = resultGsmrs!!.competition!!.season!!.round!!.resultTable!!.rankings!!.sortedBy { it.rank }
                    ifViewAttached { view -> view.onRankingLoaded(sortedRanking) }
                }, { throwable ->
                    throwable.printStackTrace()
                    this.handlingError(throwable)
                })
    }

}