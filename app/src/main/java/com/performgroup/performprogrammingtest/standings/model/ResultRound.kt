package com.performgroup.performprogrammingtest.standings.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "round", strict = false)
data class ResultRound(

        @get:Attribute(name = "round_id")
        @set:Attribute(name = "round_id")
        var roundId: Long? = null,

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @get:Attribute(name = "start_date")
        @set:Attribute(name = "start_date")
        var startDate: String? = null,

        @get:Attribute(name = "end_date")
        @set:Attribute(name = "end_date")
        var endDate: String? = null,

        @get:Attribute(name = "type")
        @set:Attribute(name = "type")
        var type: String? = null,

        @get:Attribute(name = "ordermethod")
        @set:Attribute(name = "ordermethod")
        var orderMethod: Int? = null,

        @get:Attribute(name = "groups")
        @set:Attribute(name = "groups")
        var groups: Int? = null,

        @get:Attribute(name = "has_outgroup_matches")
        @set:Attribute(name = "has_outgroup_matches")
        var hasOutgroupMatches: String? = null,

        @field:Element(name = "resultstable")
        var resultTable: ResultTable? = null

)