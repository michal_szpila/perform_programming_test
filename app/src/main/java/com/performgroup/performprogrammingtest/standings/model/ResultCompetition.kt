package com.performgroup.performprogrammingtest.standings.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "competition", strict = false)
data class ResultCompetition(
        @get:Attribute(name = "competition_id")
        @set:Attribute(name = "competition_id")
        var competitionId: Long? = null,

        @get:Attribute(name = "name")
        @set:Attribute(name = "name")
        var name: String? = null,

        @get:Attribute(name = "teamtype")
        @set:Attribute(name = "teamtype")
        var teamType: String? = null,

        @get:Attribute(name = "display_order")
        @set:Attribute(name = "display_order")
        var displayOrder: Int? = null,

        @get:Attribute(name = "type")
        @set:Attribute(name = "type")
        var type: String? = null,

        @get:Attribute(name = "area_id")
        @set:Attribute(name = "area_id")
        var areaId: Int? = null,

        @get:Attribute(name = "area_name")
        @set:Attribute(name = "area_name")
        var areaName: String? = null,

        @get:Attribute(name = "last_updated")
        @set:Attribute(name = "last_updated")
        var lastUpdated: String? = null,

        @get:Attribute(name = "soccertype")
        @set:Attribute(name = "soccertype")
        var soccerType: String? = null,

        @field:Element(name = "season")
        var season: ResultSeason? = null
)