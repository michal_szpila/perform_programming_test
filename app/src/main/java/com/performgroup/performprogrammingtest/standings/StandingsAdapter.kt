package com.performgroup.performprogrammingtest.standings

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseAdapter
import com.performgroup.performprogrammingtest.standings.model.Ranking
import kotlinx.android.synthetic.main.recycler_view_standings_row.view.*

class StandingsAdapter : BaseAdapter<StandingsAdapter.StandingsViewHolder, Ranking>() {

    companion object {
        const val LAST_CHAMPIONS_LEAGUE_POSITION = 3
        const val EUROPA_LEAGUE_POSITION = 4
        const val FIRST_RELEGATION_POSITION: Int = 17
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): StandingsViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_standings_row, parent, false)
        return StandingsViewHolder(view)
    }

    override fun onBindViewHolder(holder: StandingsViewHolder, position: Int) {
        holder.bind(this.modelList[position], position)
    }

    class StandingsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(ranking: Ranking, position: Int) {

            this.itemView.standings_row_position.text = String.format("%02d", ranking.rank)
            this.itemView.standings_row_team.text = ranking.clubName
            this.itemView.standings_row_matches_total.text = ranking.matchesTotal.toString()
            this.itemView.standings_row_matches_won.text = ranking.matchesWon.toString()
            this.itemView.standings_row_matches_draw.text = ranking.matchesDraw.toString()
            this.itemView.standings_row_matches_lost.text = ranking.matchesLost.toString()
            this.itemView.standings_row_goal_difference.text = (ranking.goalsPro!! - ranking.goalsAgainst!!).toString()
            this.itemView.standings_row_points.text = ranking.points.toString()

            if (position % 2 == 0) {
                this.itemView.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorFirstRow))
            } else {
                this.itemView.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorSecondRow))
            }

            when {
                position <= LAST_CHAMPIONS_LEAGUE_POSITION -> {
                    this.itemView.standings_row_position.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorChampionsLeague))
                    this.itemView.standings_row_position.setTextColor(ContextCompat.getColor(this.itemView.context, android.R.color.white))
                }
                position == EUROPA_LEAGUE_POSITION -> {
                    this.itemView.standings_row_position.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorEuropaLeague))
                    this.itemView.standings_row_position.setTextColor(ContextCompat.getColor(this.itemView.context, android.R.color.white))
                }
                position >= FIRST_RELEGATION_POSITION -> {
                    this.itemView.standings_row_position.setBackgroundColor(ContextCompat.getColor(this.itemView.context, R.color.colorDownward))
                    this.itemView.standings_row_position.setTextColor(ContextCompat.getColor(this.itemView.context, android.R.color.white))
                }
                else -> {
                    this.itemView.standings_row_position.setBackgroundColor(ContextCompat.getColor(this.itemView.context, android.R.color.transparent))
                    this.itemView.standings_row_position.setTextColor(ContextCompat.getColor(this.itemView.context, R.color.colorAccent))
                }
            }

            val positionDifference: Int = ranking.rank!! - ranking.lastRank!!
            when {
                positionDifference == 0 -> this.itemView.standings_row_image_arrow.setImageResource(R.drawable.ic_same_position)
                positionDifference > 0 -> this.itemView.standings_row_image_arrow.setImageResource(R.drawable.ic_down_red)
                else -> this.itemView.standings_row_image_arrow.setImageResource(R.drawable.ic_up_green)
            }

        }

    }
}