package com.performgroup.performprogrammingtest.standings.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "resultstable", strict = false)
data class ResultTable(

        @get:Attribute(name = "type")
        @set:Attribute(name = "type")
        var type: String? = null,

        @field:ElementList(inline = true, entry = "ranking")
        var rankings: List<Ranking>? = null
)