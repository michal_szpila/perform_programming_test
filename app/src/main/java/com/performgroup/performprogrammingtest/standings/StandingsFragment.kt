package com.performgroup.performprogrammingtest.standings


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.PerformTestApplication
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseFragment
import com.performgroup.performprogrammingtest.dagger.NetModule
import com.performgroup.performprogrammingtest.standings.model.Ranking
import kotlinx.android.synthetic.main.fragment_standings.*


/**
 * A simple [Fragment] subclass.
 *
 */
class StandingsFragment : BaseFragment<StandingsView, StandingsPresenter>(), StandingsView {

    private lateinit var standingsAdapter: StandingsAdapter


    override fun createPresenter(): StandingsPresenter {
        return DaggerStandingsComponent.builder()
                .appComponent(PerformTestApplication.component)
                .netModule(NetModule())
                .build()
                .presenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_standings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.initAdapter()
        this.showLoader(true)
        this.presenter.getStandingsFromApi()
    }

    private fun initAdapter() {
        this.standingsAdapter = StandingsAdapter()
        this.standings_recycler_view.adapter = this.standingsAdapter
        this.standings_recycler_view.layoutManager = LinearLayoutManager(activity)
    }


    override fun showLoader(isLoading: Boolean) {
        if (isLoading) {
            this.standings_container.visibility = View.GONE
            this.standings_loader.visibility = View.VISIBLE
        } else {
            this.standings_container.visibility = View.VISIBLE
            this.standings_loader.visibility = View.GONE
        }
    }

    override fun onRankingLoaded(ranking: List<Ranking>) {
        this.standingsAdapter.addFromResponse(ranking)
        this.showLoader(false)
    }

    override fun onError() {
        this.showLoader(false)

        this.getErrorSnackbar(this.standings_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getStandingsFromApi()
        }.show()
    }

    override fun onNoInternetConnection() {
        this.showLoader(false)

        this.getNoInternetSnackbar(this.standings_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getStandingsFromApi()
        }.show()
    }


}
