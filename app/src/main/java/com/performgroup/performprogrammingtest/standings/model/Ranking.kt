package com.performgroup.performprogrammingtest.standings.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "ranking", strict = false)
data class Ranking(

        @get:Attribute(name = "rank")
        @set:Attribute(name = "rank")
        var rank: Int? = null,

        @get:Attribute(name = "last_rank")
        @set:Attribute(name = "last_rank")
        var lastRank: Int? = null,

        @get:Attribute(name = "team_id")
        @set:Attribute(name = "team_id")
        var teamId: Long? = null,

        @get:Attribute(name = "club_name")
        @set:Attribute(name = "club_name")
        var clubName: String? = null,

        @get:Attribute(name = "countrycode")
        @set:Attribute(name = "countrycode")
        var countryCode: String? = null,

        @get:Attribute(name = "area_id")
        @set:Attribute(name = "area_id")
        var areaId: Long? = null,

        @get:Attribute(name = "matches_total")
        @set:Attribute(name = "matches_total")
        var matchesTotal: Int? = null,

        @get:Attribute(name = "matches_won")
        @set:Attribute(name = "matches_won")
        var matchesWon: Int? = null,

        @get:Attribute(name = "matches_draw")
        @set:Attribute(name = "matches_draw")
        var matchesDraw: Int? = null,

        @get:Attribute(name = "matches_lost")
        @set:Attribute(name = "matches_lost")
        var matchesLost: Int? = null,

        @get:Attribute(name = "goals_pro")
        @set:Attribute(name = "goals_pro")
        var goalsPro: Int? = null,

        @get:Attribute(name = "goals_against")
        @set:Attribute(name = "goals_against")
        var goalsAgainst: Int? = null,

        @get:Attribute(name = "points")
        @set:Attribute(name = "points")
        var points: Int? = null

)