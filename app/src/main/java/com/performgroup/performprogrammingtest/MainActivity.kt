package com.performgroup.performprogrammingtest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import com.anthonyfdev.dropdownview.DropDownView
import com.performgroup.performprogrammingtest.navigation.MenuActionsDelegate
import com.performgroup.performprogrammingtest.navigation.MenuDropDownAdapter
import com.performgroup.performprogrammingtest.news.NewsFragment
import com.performgroup.performprogrammingtest.scores.ScoresFragment
import com.performgroup.performprogrammingtest.standings.StandingsFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drop_down_menu_expanded.view.*
import kotlinx.android.synthetic.main.drop_down_menu_header.view.*


class MainActivity : AppCompatActivity(), MenuActionsDelegate, DropDownView.DropDownListener {


    private lateinit var menuDropDownAdapter: MenuDropDownAdapter

    private var selectedDropDownMenuId: Int = -1

    private lateinit var collapsedView: View
    private lateinit var expandedView: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.supportActionBar!!.hide()

        this.initDropDownMenu()
    }

    private fun initDropDownMenu() {
        this.collapsedView = LayoutInflater.from(this).inflate(R.layout.drop_down_menu_header, this.main_drop_down_view, false)
        this.expandedView = LayoutInflater.from(this).inflate(R.layout.drop_down_menu_expanded, this.main_drop_down_view, false)

        this.menuDropDownAdapter = MenuDropDownAdapter(this)
        this.expandedView.drop_down_menu_expanded_recycler_view.layoutManager = LinearLayoutManager(this)
        this.expandedView.drop_down_menu_expanded_recycler_view.adapter = this.menuDropDownAdapter

        this.main_drop_down_view.setHeaderView(this.collapsedView)
        this.main_drop_down_view.setExpandedView(this.expandedView)
        this.main_drop_down_view.dropDownListener = this

        this.setSelectedStand(0)
    }

    private fun displayFragment(indexOfFragment: Int) {

        val fragment: Fragment = when (indexOfFragment) {
            0 -> NewsFragment()
            1 -> ScoresFragment()
            else -> StandingsFragment()
        }

        this.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, fragment)
                .commit()
    }


    override fun onCollapseDropDown() {
        this.menuDropDownAdapter.notifyDataSetChanged()
        this.collapsedView.drop_down_menu_header_image.animate()
                .rotation(0.0f)
                .start()

        this.setHeightOfMenuBackground()
    }

    override fun onExpandDropDown() {
        this.collapsedView.drop_down_menu_header_image.animate()
                .rotation(180.0f)
                .start()

        this.setHeightOfMenuBackground()

    }

    private fun setHeightOfMenuBackground() {
        val view: View = this.main_drop_down_view.findViewById(R.id.empty_drop_down_space)
        view.layoutParams.height = 0
    }


    override fun collapseDropDown() {
        this.main_drop_down_view.collapseDropDown()
    }

    override fun setSelectedStand(standId: Int) {
        if (standId != this.selectedDropDownMenuId) {
            this.collapsedView.drop_down_menu_header_title.text = this.getStandTitle(standId)
            this.selectedDropDownMenuId = standId
            this.displayFragment(standId)
        }
    }

    override fun getStandTitle(standId: Int): String {

        when (standId) {
            0 -> return getString(R.string.menu_news_item)
            1 -> return getString(R.string.menu_scores_item)
            2 -> return getString(R.string.menu_standings_item)
        }

        return ""

    }

    override fun getSelectedStand() = this.selectedDropDownMenuId

    override fun onBackPressed() {
        when {
            this.main_drop_down_view.isExpanded -> {
                this.main_drop_down_view.collapseDropDown()
            }
            this.selectedDropDownMenuId == 0 -> AlertDialog.Builder(this)
                    .setMessage(R.string.exit_app_question)
                    .setPositiveButton(R.string.exit) { _, _ ->
                        this.finish()
                    }
                    .setNegativeButton(R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            else -> this.setSelectedStand(0)
        }
    }

}
