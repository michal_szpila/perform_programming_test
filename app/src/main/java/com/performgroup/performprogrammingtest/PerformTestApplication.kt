package com.performgroup.performprogrammingtest

import android.app.Application
import com.performgroup.performprogrammingtest.dagger.AppComponent
import com.performgroup.performprogrammingtest.dagger.DaggerAppComponent

class PerformTestApplication : Application() {

    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
                .application(this)
                .build()
    }


}