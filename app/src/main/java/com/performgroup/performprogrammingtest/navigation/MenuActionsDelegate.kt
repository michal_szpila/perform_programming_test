package com.performgroup.performprogrammingtest.navigation

interface MenuActionsDelegate {

    fun collapseDropDown()

    fun setSelectedStand(standId: Int)

    fun getStandTitle(standId: Int): String

    fun getSelectedStand(): Int
}