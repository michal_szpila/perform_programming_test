package com.performgroup.performprogrammingtest.navigation

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.R
import kotlinx.android.synthetic.main.drop_down_menu_item.*
import kotlinx.android.synthetic.main.drop_down_menu_item.view.*


class MenuDropDownAdapter(private val menuActionsDelegate: MenuActionsDelegate) : RecyclerView.Adapter<MenuDropDownAdapter.MenuItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MenuItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.drop_down_menu_item, parent, false)
        return MenuItemViewHolder(itemView)
    }

    override fun getItemCount() = 3

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        holder.bind(menuActionsDelegate, this, position)
    }

    class MenuItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private lateinit var menuActionsDelegate: MenuActionsDelegate
        private lateinit var menuDropDownAdapter: MenuDropDownAdapter

        override fun onClick(p0: View?) {
            val lastSelectedPosition = menuActionsDelegate.getSelectedStand()
            this.menuActionsDelegate.setSelectedStand(adapterPosition)
            this.menuDropDownAdapter.notifyItemChanged(lastSelectedPosition)
            this.menuDropDownAdapter.notifyItemChanged(adapterPosition)
            this.menuActionsDelegate.collapseDropDown()
        }

        fun bind(menuActionsDelegate: MenuActionsDelegate, menuDropDownAdapter: MenuDropDownAdapter, position: Int) {
            this.menuActionsDelegate = menuActionsDelegate
            this.menuDropDownAdapter = menuDropDownAdapter

            itemView.background = ContextCompat.getDrawable(itemView.context, R.drawable.drop_down_menu_selector)
            itemView.drop_down_menu_item_title.text = this.menuActionsDelegate.getStandTitle(position)
            itemView.isSelected = this.menuActionsDelegate.getSelectedStand() == position

            itemView.setOnClickListener(this)
        }


    }
}