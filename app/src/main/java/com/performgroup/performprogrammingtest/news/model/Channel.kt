package com.performgroup.performprogrammingtest.news.model

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "channel", strict = false)
data class Channel(

        @field:ElementList(inline = true, entry = "item")
        var items: List<Item>? = null

)

