package com.performgroup.performprogrammingtest.news

import com.performgroup.performprogrammingtest.base.BasePresenter
import com.performgroup.performprogrammingtest.dagger.PerScreen
import com.performgroup.performprogrammingtest.data.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@PerScreen
class NewsPresenter @Inject constructor(private val api: Api) : BasePresenter<NewsView>() {


    fun getNewsFromApi() {
        this.disposeApiRequest()

        this.disposable = this.api.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ rss ->
                    ifViewAttached { view -> view.onNewsLoaded(rss!!.channel!!.items!!) }
                }, { throwable ->
                    throwable.printStackTrace()
                    this.handlingError(throwable)
                })
    }


}