package com.performgroup.performprogrammingtest.news.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "enclosure", strict = false)
data class Enclosure(

        @get:Attribute(name = "length")
        @set:Attribute(name = "length")
        var length: String? = null,

        @get:Attribute(name = "type")
        @set:Attribute(name = "type")
        var type: String? = null,

        @get:Attribute(name = "url")
        @set:Attribute(name = "url")
        var url: String? = null
)