package com.performgroup.performprogrammingtest.news

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseAdapter
import com.performgroup.performprogrammingtest.news.model.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recycler_view_news_row.view.*

class NewsAdapter : BaseAdapter<NewsAdapter.NewsViewHolder, Item>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NewsViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_news_row, parent, false)
        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(this.modelList[position])
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private lateinit var item: Item

        override fun onClick(p0: View?) {
            val intent = Intent(this.itemView.context, NewsPreviewActivity::class.java)
            intent.putExtra(NewsPreviewActivity.NEWS_PREVIEW_LINK_KEY, this.item.link)
            intent.putExtra(NewsPreviewActivity.NEWS_PREVIEW_TITLE_KEY, this.item.title)
            this.itemView.context.startActivity(intent)
        }

        fun bind(item: Item) {
            this.item = item

            this.itemView.setOnClickListener(this)

            this.itemView.news_row_title.text = this.item.title
            this.itemView.news_row_date.text = this.item.pubDate!!.replace("+0000", "")

            Picasso.get()
                    .load(this.item.enclosure!!.url)
                    .fit()
                    .centerCrop()
                    .into(this.itemView.news_row_image)

        }


    }
}