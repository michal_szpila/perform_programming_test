package com.performgroup.performprogrammingtest.news


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.performgroup.performprogrammingtest.PerformTestApplication
import com.performgroup.performprogrammingtest.R
import com.performgroup.performprogrammingtest.base.BaseFragment
import com.performgroup.performprogrammingtest.dagger.NetModule
import com.performgroup.performprogrammingtest.news.model.Item
import kotlinx.android.synthetic.main.fragment_news.*


/**
 * A simple [Fragment] subclass.
 *
 */
class NewsFragment : BaseFragment<NewsView, NewsPresenter>(), NewsView {

    private lateinit var newsAdapter: NewsAdapter

    override fun createPresenter(): NewsPresenter {
        return DaggerNewsComponent.builder()
                .appComponent(PerformTestApplication.component)
                .netModule(NetModule())
                .build()
                .presenter()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    private fun initAdapter() {
        this.newsAdapter = NewsAdapter()
        this.news_recycler_view.layoutManager = LinearLayoutManager(activity)
        this.news_recycler_view.addItemDecoration(DividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
        this.news_recycler_view.adapter = this.newsAdapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.initAdapter()
        this.presenter.getNewsFromApi()
    }

    override fun showLoader(isLoading: Boolean) {
        if (isLoading) {
            this.news_recycler_view.visibility = View.GONE
            this.news_loader.visibility = View.VISIBLE
        } else {
            this.news_recycler_view.visibility = View.VISIBLE
            this.news_loader.visibility = View.GONE
        }
    }

    override fun onNewsLoaded(news: List<Item>) {
        this.newsAdapter.addFromResponse(news)

        this.showLoader(false)
    }


    override fun onError() {
        this.showLoader(false)

        this.getErrorSnackbar(this.news_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getNewsFromApi()
        }.show()
    }

    override fun onNoInternetConnection() {
        this.showLoader(false)

        this.getNoInternetSnackbar(this.news_root_view).setAction(R.string.retry) {
            this.showLoader(true)
            this.presenter.getNewsFromApi()
        }.show()
    }


}
