package com.performgroup.performprogrammingtest.news.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "rss", strict = false)
data class Rss(

        @get:Element(name = "channel")
        @set:Element(name = "channel")
        var channel: Channel? = null
)