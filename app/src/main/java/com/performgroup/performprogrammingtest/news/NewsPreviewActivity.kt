package com.performgroup.performprogrammingtest.news

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.performgroup.performprogrammingtest.R
import kotlinx.android.synthetic.main.activity_news_preview.*

class NewsPreviewActivity : AppCompatActivity() {

    companion object {
        const val NEWS_PREVIEW_LINK_KEY = "news_preview_link_key"
        const val NEWS_PREVIEW_TITLE_KEY = "news_preview_title"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_preview)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        this.initWebView()
    }

    private fun initWebView() {
        supportActionBar!!.title = intent.getStringExtra(NEWS_PREVIEW_TITLE_KEY)

        val url = intent.getStringExtra(NEWS_PREVIEW_LINK_KEY)
        this.news_preview_webview.loadUrl(url)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
