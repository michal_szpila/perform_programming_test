package com.performgroup.performprogrammingtest.news.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class Item(

        @get:Element(name = "guid")
        @set:Element(name = "guid")
        var guid: String? = null,

        @get:Element(name = "pubDate")
        @set:Element(name = "pubDate")
        var pubDate: String? = null,

        @get:Element(name = "title")
        @set:Element(name = "title")
        var title: String? = null,

        @get:Element(name = "category")
        @set:Element(name = "category")
        var category: String? = null,

        @get:Element(name = "enclosure")
        @set:Element(name = "enclosure")
        var enclosure: Enclosure? = null,

        @get:Element(name = "description")
        @set:Element(name = "description")
        var description: String? = null,

        @get:Element(name = "link")
        @set:Element(name = "link")
        var link: String? = null
)