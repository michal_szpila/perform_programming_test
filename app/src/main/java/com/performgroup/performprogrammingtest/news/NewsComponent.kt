package com.performgroup.performprogrammingtest.news

import com.performgroup.performprogrammingtest.dagger.AppComponent
import com.performgroup.performprogrammingtest.dagger.NetModule
import com.performgroup.performprogrammingtest.dagger.PerScreen
import dagger.Component

@PerScreen
@Component(modules = [NetModule::class],
        dependencies = [AppComponent::class])
interface NewsComponent {
    fun presenter(): NewsPresenter
}