package com.performgroup.performprogrammingtest.news

import com.performgroup.performprogrammingtest.base.ApiView
import com.performgroup.performprogrammingtest.news.model.Item

interface NewsView : ApiView {

    fun onNewsLoaded(news: List<Item>)
}