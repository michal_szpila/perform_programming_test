package com.performgroup.performprogrammingtest

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.isDialog
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import com.azimolabs.conditionwatcher.ConditionWatcher
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    public var activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


    /**
     * Check if drop down menu is expanded after click on header
     */
    @Test
    fun testExpandDropDownMenu() {
        clickOnDropDownMenu()

        onView(withId(R.id.drop_down_menu_expanded_recycler_view))
                .check(matches(isDisplayed()))
    }

    /**
     * Check if drop down menu is collapsed after second click on header
     */
    @Test
    fun testCollapseDropDownMenu() {
        clickOnDropDownMenu()

        Thread.sleep(2000)

        clickOnDropDownMenu()

        Thread.sleep(2000)

        onView(withId(R.id.drop_down_menu_expanded_recycler_view))
                .check(matches(not(isDisplayed())))
    }

    /**
     * Check if Scores screen is opening from drop down menu
     */
    @Test
    fun testShowScoresScreen() {
        openScoresScreen()

        onView(withId(R.id.scores_root_view))
                .check(matches(isDisplayed()))
    }

    /**
     * Check if Standings screen is opening from drop down menu
     */
    @Test
    fun testShowStandingsScreen() {
        openStandingsScreen()

        onView(withId(R.id.standings_root_view))
                .check(matches(isDisplayed()))
    }

    /**
     * Check if News screen is opening from drop down menu
     */
    @Test
    fun testShowNewsScreen() {
        openStandingsScreen()

        openNewsScreen()

        onView(withId(R.id.news_root_view))
                .check(matches(isDisplayed()))
    }

    /**
     * Check if News screen is opened after back pressed on Scores screen
     */
    @Test
    fun testBackFromScoresScreen() {
        openScoresScreen()

        Espresso.pressBack()

        onView(withId(R.id.news_root_view))
                .check(matches(isDisplayed()))
    }

    /**
     * Check if News screen is opened after back pressed on Standings screen
     */
    @Test
    fun testBackFromStandingsScreen() {
        openStandingsScreen()

        Espresso.pressBack()

        onView(withId(R.id.news_root_view))
                .check(matches(isDisplayed()))
    }

    /**
     * CHeck if News preview screen is showing after click on news on list
     */
    @Test
    fun testOpenNewsPreview() {
        Thread.sleep(1000)
        ConditionWatcher.waitForCondition(LoadingNewsInstruction(activityTestRule.activity))

        Thread.sleep(1000)
        onView(allOf(withId(R.id.news_recycler_view), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        Thread.sleep(1000)

        onView(withId(R.id.news_preview_webview))
                .check(matches(isDisplayed()))
    }


    /**
     * Check if alert dialog with exit information is showing
     */
    @Test
    fun testOpenExitAppAlertDialog() {
        openExitAppAlertDialog()
    }

    /**
     * Check if activity is finishing after click "Exit" button in alert dialog
     */
    @Test
    fun testExitApplication() {
        openExitAppAlertDialog()

        onView(withId(android.R.id.button1)).perform(click())

        assertTrue(activityTestRule.activity.isFinishing)

    }


    private fun openExitAppAlertDialog() {
        Espresso.pressBack()

        onView(withText(R.string.exit_app_question))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
    }

    private fun clickOnDropDownMenu() {
        onView(withId(R.id.drop_down_menu_header_title))
                .perform(click())
    }

    private fun openScoresScreen() {
        clickOnDropDownMenu()

        onView(allOf(withId(R.id.drop_down_menu_expanded_recycler_view), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(hasDescendant(withText(R.string.menu_scores_item)), click()))

    }

    private fun openStandingsScreen() {
        clickOnDropDownMenu()

        onView(allOf(withId(R.id.drop_down_menu_expanded_recycler_view), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(hasDescendant(withText(R.string.menu_standings_item)), click()))

    }

    private fun openNewsScreen() {
        clickOnDropDownMenu()

        onView(allOf(withId(R.id.drop_down_menu_expanded_recycler_view), isDisplayed()))
                .perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(hasDescendant(withText(R.string.menu_news_item)), click()))

    }

}