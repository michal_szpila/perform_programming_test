package com.performgroup.performprogrammingtest

import android.support.v7.widget.RecyclerView
import android.view.View
import com.azimolabs.conditionwatcher.Instruction


class LoadingNewsInstruction(var mainActivity: MainActivity) : Instruction() {

    override fun getDescription(): String {
        return ""
    }

    override fun checkCondition(): Boolean {
        val recyclerView: RecyclerView = mainActivity.findViewById(R.id.news_recycler_view)

        return recyclerView.visibility == View.VISIBLE
    }
}