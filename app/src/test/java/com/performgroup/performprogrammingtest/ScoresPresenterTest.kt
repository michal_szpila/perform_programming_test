package com.performgroup.performprogrammingtest

import com.performgroup.performprogrammingtest.data.Api
import com.performgroup.performprogrammingtest.scores.ScoresPresenter
import com.performgroup.performprogrammingtest.scores.ScoresView
import com.performgroup.performprogrammingtest.scores.model.*
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.net.SocketTimeoutException

class ScoresPresenterTest {

    private lateinit var scoresPresenter: ScoresPresenter

    @Mock
    private lateinit var scoresView: ScoresView

    @Mock
    private lateinit var api: Api

    @Before
    fun setUp() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.computation() }

        MockitoAnnotations.initMocks(this)

        scoresPresenter = ScoresPresenter(api)
        scoresPresenter.attachView(scoresView)
    }

    @Test
    fun testApiNotNull() {
        Assert.assertNotNull(api)
    }

    @Test
    fun testLoadScores() {
        val gsmrs = Mockito.spy(Gsmrs::class.java)

        Mockito.`when`(gsmrs.method).thenReturn(Mockito.spy(Method::class.java))
        Mockito.`when`(gsmrs.competition).thenReturn(Mockito.spy(Competition::class.java))
        Mockito.`when`(gsmrs.competition!!.season).thenReturn(Mockito.spy(Season::class.java))
        Mockito.`when`(gsmrs.competition!!.season!!.round).thenReturn(Mockito.spy(Round::class.java))

        val parametersList = ArrayList<Paramater>()
        val date = Paramater("date", "1.10.2018")
        parametersList.add(date)
        gsmrs.method!!.paramaters = parametersList

        val group = Mockito.spy(Group::class.java)
        group.matches = ArrayList()

        val groupList = ArrayList<Group>()
        groupList.add(group)
        gsmrs.competition!!.season!!.round!!.groupList = groupList

        Mockito.`when`(api.getScores()).thenReturn(Observable.just(gsmrs))
        scoresPresenter.getScoresFromApi()
        Mockito.verify(scoresView, Mockito.timeout(200)).onMatchesLoaded(scoresPresenter.getDateFromGsmrs(gsmrs), scoresPresenter.getMatchListFromGsmrs(gsmrs))
    }

    @Test
    fun testRefresh() {
        scoresPresenter.refresh()

        // waiting 30 seconds for refresh
        Thread.sleep(30000)
        Mockito.verify(scoresView, Mockito.timeout(200)).onStartFetchingData(true)
    }

    @Test
    fun testDateFromGsmrs() {
        val gsmrs = Mockito.spy(Gsmrs::class.java)
        Mockito.`when`(gsmrs.method).thenReturn(Mockito.spy(Method::class.java))

        val parametersList = ArrayList<Paramater>()

        val parameter1 = Paramater("test", "test")
        val parameter2 = Paramater("date", "1.10.2018")
        val parameter3 = Paramater("", "16.09.2018")

        parametersList.add(parameter1)
        parametersList.add(parameter2)
        parametersList.add(parameter3)

        gsmrs.method!!.paramaters = parametersList

        Assert.assertEquals(scoresPresenter.getDateFromGsmrs(gsmrs), parameter2.value)
    }

    @Test
    fun testMatchesList() {
        val gsmrs = Mockito.spy(Gsmrs::class.java)
        Mockito.`when`(gsmrs.competition).thenReturn(Mockito.spy(Competition::class.java))
        Mockito.`when`(gsmrs.competition!!.season).thenReturn(Mockito.spy(Season::class.java))
        Mockito.`when`(gsmrs.competition!!.season!!.round).thenReturn(Mockito.spy(Round::class.java))

        val match1 = Mockito.mock(Match::class.java)
        val match2 = Mockito.mock(Match::class.java)

        val group1 = Mockito.spy(Group::class.java)
        group1.name = "group1"
        val matchesGroup1 = ArrayList<Match>()
        matchesGroup1.add(match1)
        matchesGroup1.add(match2)
        group1.matches = matchesGroup1

        val match3 = Mockito.mock(Match::class.java)
        val match4 = Mockito.mock(Match::class.java)
        val match5 = Mockito.mock(Match::class.java)

        val group2 = Mockito.spy(Group::class.java)
        group2.name = "group2"
        val matchesGroup2 = ArrayList<Match>()
        matchesGroup2.add(match3)
        matchesGroup2.add(match4)
        matchesGroup2.add(match5)
        group2.matches = matchesGroup2

        val groupList = ArrayList<Group>()
        groupList.add(group1)
        groupList.add(group2)

        gsmrs.competition!!.season!!.round!!.groupList = groupList

        Assert.assertEquals(scoresPresenter.getMatchListFromGsmrs(gsmrs).size, 5)

    }


    @Test
    fun testInternetConnectionError() {
        Mockito.`when`(api.getScores()).thenReturn(Observable.error(SocketTimeoutException()))
        scoresPresenter.getScoresFromApi()
        Mockito.verify(scoresView, Mockito.timeout(200)).onNoInternetConnection()
    }


    @Test
    fun testError() {
        Mockito.`when`(api.getScores()).thenReturn(Observable.error(RuntimeException()))
        scoresPresenter.getScoresFromApi()
        Mockito.verify(scoresView, Mockito.timeout(200)).onError()
    }

    @After
    fun after() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()

        Mockito.reset(scoresView)
        Mockito.reset(api)
        Mockito.validateMockitoUsage()

    }

}