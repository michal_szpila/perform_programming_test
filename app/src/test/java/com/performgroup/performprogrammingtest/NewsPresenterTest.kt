package com.performgroup.performprogrammingtest

import com.performgroup.performprogrammingtest.data.Api
import com.performgroup.performprogrammingtest.news.NewsPresenter
import com.performgroup.performprogrammingtest.news.NewsView
import com.performgroup.performprogrammingtest.news.model.Channel
import com.performgroup.performprogrammingtest.news.model.Item
import com.performgroup.performprogrammingtest.news.model.Rss
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.net.SocketTimeoutException


class NewsPresenterTest {

    private lateinit var newsPresenter: NewsPresenter

    @Mock
    private lateinit var newsView: NewsView

    @Mock
    private lateinit var api: Api


    @Before
    fun setUp() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.computation() }

        MockitoAnnotations.initMocks(this)

        newsPresenter = NewsPresenter(api)
        newsPresenter.attachView(newsView)

    }


    @Test
    fun testApiNotNull() {
        Assert.assertNotNull(api)
    }


    @Test
    fun testLoadNews() {
        val rss = Rss()
        val list = ArrayList<Item>()
        rss.channel = Channel(list)

        `when`(api.getNews()).thenReturn(Observable.just(rss))
        newsPresenter.getNewsFromApi()
        verify(newsView, timeout(200)).onNewsLoaded(list)
    }

    @Test
    fun testInternetConnectionError() {
        `when`(api.getNews()).thenReturn(Observable.error(SocketTimeoutException()))
        newsPresenter.getNewsFromApi()
        verify(newsView, timeout(200)).onNoInternetConnection()
    }


    @Test
    fun testError() {
        `when`(api.getNews()).thenReturn(Observable.error(RuntimeException()))
        newsPresenter.getNewsFromApi()
        verify(newsView, timeout(200)).onError()
    }

    @After
    fun after() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()

        Mockito.reset(newsView)
        Mockito.reset(api)
        Mockito.validateMockitoUsage()

    }

}