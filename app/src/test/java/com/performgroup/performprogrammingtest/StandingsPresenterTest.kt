package com.performgroup.performprogrammingtest

import com.performgroup.performprogrammingtest.data.Api
import com.performgroup.performprogrammingtest.standings.StandingsPresenter
import com.performgroup.performprogrammingtest.standings.StandingsView
import com.performgroup.performprogrammingtest.standings.model.*
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.net.SocketTimeoutException

class StandingsPresenterTest {

    private lateinit var standingsPresenter: StandingsPresenter

    @Mock
    private lateinit var standingsView: StandingsView

    @Mock
    private lateinit var api: Api


    @Before
    fun setUp() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.computation() }

        MockitoAnnotations.initMocks(this)

        standingsPresenter = StandingsPresenter(api)
        standingsPresenter.attachView(standingsView)

    }


    @Test
    fun testApiNotNull() {
        Assert.assertNotNull(api)
    }


    @Test
    fun testLoadStandings() {
        val resultGsmrs = ResultGsmrs()
        val competition = ResultCompetition()
        val season = ResultSeason()
        val round = ResultRound()
        val resultTable = ResultTable()

        val rankingList = ArrayList<Ranking>()

        resultTable.rankings = rankingList
        round.resultTable = resultTable
        season.round = round
        competition.season = season
        resultGsmrs.competition = competition

        val ranking = Ranking()
        ranking.rank = 2
        rankingList.add(ranking)

        val ranking2 = Ranking()
        ranking.rank = 1
        rankingList.add(ranking2)

        rankingList.sortBy { it.rank }

        Mockito.`when`(api.getStandings()).thenReturn(Observable.just(resultGsmrs))
        standingsPresenter.getStandingsFromApi()

        // waiting for sorting in presenter
        Thread.sleep(1000)

        Mockito.verify(standingsView, Mockito.timeout(200)).onRankingLoaded(rankingList)

    }

    @Test
    fun testInternetConnectionError() {
        Mockito.`when`(api.getStandings()).thenReturn(Observable.error(SocketTimeoutException()))
        standingsPresenter.getStandingsFromApi()
        Mockito.verify(standingsView, Mockito.timeout(200)).onNoInternetConnection()
    }


    @Test
    fun testError() {
        Mockito.`when`(api.getStandings()).thenReturn(Observable.error(RuntimeException()))
        standingsPresenter.getStandingsFromApi()
        Mockito.verify(standingsView, Mockito.timeout(200)).onError()
    }

    @After
    fun after() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()

        Mockito.reset(standingsView)
        Mockito.reset(api)
        Mockito.validateMockitoUsage()

    }
}