# README #

**Perform - Experienced Developer Programming Test**

* Application was created in Model-View-Presenter (MVP) pattern
* Presenter methods are covered with unit tests (using JUnit and Mockito)
* There are also a few UI tests (Espresso)
